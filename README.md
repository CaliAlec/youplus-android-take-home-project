# Youplus Android Take Home Project #

Basic Android app that shows a list of conversations.

### What does it do? ###

* On initial run, 5 dummy conversations are generated.
* When you click the plus FAB, a new conversation is created. The new conversation item has a spinning progress bar loader that lasts for 1.5 seconds and then the new conversation data is shown after.
* You can reset the conversation list back to the initial dummy conversations by using the menu item in the top right of the Toolbar.
* The conversations are persisted using [Realm](https://realm.io/). The conversation list is populated by the results of a Realm query which also sorts the conversations so the newest conversation is at the top.

[![GIF Demo](https://thumbs.gfycat.com/WarmheartedVagueBovine-size_restricted.gif)](https://gfycat.com/WarmheartedVagueBovine)