package calialec.com.youplus;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;

public class ConversationsAdapter extends
        RecyclerView.Adapter<ConversationsAdapter.ViewHolder> {

    public static final int LOADING_TIMEOUT = 1500;
    private List<Conversation> mConversations;
    private Context mContext;
    private Realm realm;

    public ConversationsAdapter(Context context, List<Conversation> conversations) {
        mConversations = conversations;
        mContext = context;
        realm = Realm.getDefaultInstance();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public TextView messageTextView;
        public TextView timestampTextView;
        public ProgressBar progressBar;
        public View conversationContent;

        public ViewHolder(View itemView) {
            super(itemView);

            conversationContent = itemView.findViewById(R.id.conversation_content);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);

            nameTextView = (TextView) conversationContent.findViewById(R.id.conversation_user_name);
            messageTextView = (TextView) conversationContent.findViewById(R.id.conversation_message);
            timestampTextView = (TextView) conversationContent.findViewById(R.id.conversation_timestamp);
        }
    }

    @Override
    public ConversationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View conversationView = inflater.inflate(R.layout.item_conversation, parent, false);

        ViewHolder viewHolder = new ViewHolder(conversationView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ConversationsAdapter.ViewHolder viewHolder, int position) {
        final Conversation conversation = mConversations.get(position);

        if (conversation.isLoading()) {
            // hide conversation content and show loader
            viewHolder.progressBar.setVisibility(View.VISIBLE);
            viewHolder.conversationContent.setVisibility(View.INVISIBLE);

            // show conversation content and hide loader after 1.5 seconds
            viewHolder.progressBar.postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewHolder.progressBar.setVisibility(View.INVISIBLE);
                    viewHolder.conversationContent.setVisibility(View.VISIBLE);
                    // update conversation in the database, setting isLoading to false
                    realm.beginTransaction();
                    conversation.setLoading(false);
                    realm.commitTransaction();
                }
            }, LOADING_TIMEOUT);

        }

        TextView name = viewHolder.nameTextView;
        TextView message = viewHolder.messageTextView;
        TextView timestamp = viewHolder.timestampTextView;

        name.setText(conversation.getUser().getName());
        message.setText(conversation.getMessage());

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(conversation.getTimestamp());
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        timestamp.setText(String.format(Locale.getDefault(), "%1d:%02d",
                (hourOfDay == 0 || hourOfDay == 12) ? 12 : hourOfDay % 12, minute)
                + ((hourOfDay >= 12) ? " PM" : " AM"));

    }

    @Override
    public int getItemCount() {
        return mConversations.size();
    }

}