package calialec.com.youplus;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;

public class MainActivity extends AppCompatActivity {

    private Realm realm;
    private RecyclerView rvConversations;
    private ConversationsAdapter adapter;
    private RealmResults<Conversation> conversations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRealm();
        loadConversations();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewConversation();
                adapter.notifyItemInserted(0);
                rvConversations.scrollToPosition(0);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_reset) {
            realm.beginTransaction();
            conversations.deleteAllFromRealm();
            realm.commitTransaction();
            adapter.notifyDataSetChanged();
            loadConversations();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initRealm() {
        RealmConfiguration realmConfig = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(realmConfig);
        this.realm = Realm.getDefaultInstance();
    }

    private void loadConversations() {
        conversations = realm.where(Conversation.class).findAll();
        conversations = conversations.sort("timestamp", Sort.DESCENDING);

        // generate dummy data if the database is empty
        if (conversations.size() == 0) {
            generateInitialConversations();
        }

        rvConversations = (RecyclerView) findViewById(R.id.rvConversations);
        adapter = new ConversationsAdapter(this, conversations);
        rvConversations.setAdapter(adapter);
        rvConversations.setLayoutManager(new LinearLayoutManager(this));
    }

    private void generateInitialConversations() {
        String[] names = {"kidloop", "nightwith", "alec", "amanda", "ethan"};
        String[] messages = {"I want to add you as a friend", "Test again", "What's up?", "Hi!", "Hello."};
        long[] timestamps = {1470438426000L, 1470420512000L, 1470417326000L, 1470412604000L, 1470405780000L};

        realm.beginTransaction();
        for (int i = 0; i < 5; i++) {
            Conversation conversation = new Conversation();
            User user = new User();

            user.setId(getIdForObject(User.class));
            user.setName(names[i]);
            conversation.setId(getIdForObject(Conversation.class));
            conversation.setUser(user);
            conversation.setMessage(messages[i]);
            conversation.setTimestamp(timestamps[i]);

            realm.copyToRealm(conversation);
        }
        realm.commitTransaction();
    }

    private void addNewConversation() {
        realm.beginTransaction();
        Conversation conversation = new Conversation();
        User user = new User();

        int id = getIdForObject(Conversation.class);
        user.setId(getIdForObject(User.class));
        user.setName("user" + id);
        conversation.setId(id);
        conversation.setUser(user);
        conversation.setMessage("message" + id);
        conversation.setTimestamp(System.currentTimeMillis());
        // set loading to true so the new item has a progress bar
        conversation.setLoading(true);

        realm.copyToRealm(conversation);
        realm.commitTransaction();
    }

    // returns an id for a new object (id of the last known object + 1)
    private int getIdForObject(Class classToQuery) {
        Number lastId = realm.where(classToQuery).findAll().max("id");
        if (lastId == null) {
            lastId = 0;
        }
        return lastId.intValue() + 1;
    }

}
